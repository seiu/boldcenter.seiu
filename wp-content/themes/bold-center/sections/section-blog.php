<?php
/**
 * Blog Section
 * 
 * @package Benevolent
 */
 
$ed_blog_date          = get_theme_mod( 'benevolent_ed_blog_date', '1' );
$blog_section_title    = get_theme_mod( 'benevolent_blog_section_title' );
$blog_section_content  = get_theme_mod( 'benevolent_blog_section_content' );
$blog_section_readmore = get_theme_mod( 'benevolent_blog_section_readmore', __( 'Read More', 'benevolent' ) );
 
if( $blog_section_title || $blog_section_content ){
?>
<!-- <header class="header">
	<div class="container">
		<div class="text">
			<?php 
                if( $blog_section_title ) echo '<h2 class="main-title">' . esc_html( $blog_section_title ) . '</h2>';
                if( $blog_section_content ) echo wpautop( esc_html( $blog_section_content ) );
            ?>
		</div>
	</div>
</header> -->
<?php } 
    
    $blog_qry = new WP_Query( array( 
        'post_type'           => 'post',
        'post_status'         => 'publish',
        'category__in' => array( 4 ),
	    // 'order'   => 'ASC',
        'posts_per_page'      => 100,
        'ignore_sticky_posts' => true    
    ) );
    if( $blog_qry->have_posts() ){
    ?>    
    <div class="blog-holder">
    	<div class="container flex-container wrap">
    
    			<?php
                while( $blog_qry->have_posts() ){
                    $blog_qry->the_post();
                ?>
             
            <div class="post flex-item">

            <div class="image is-style-circle-mask">
            <a href="#post-<?php the_ID(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" id="img<?php the_ID(); ?>"></a>
            </div>

            <div class="text-holder">
            <header class="entry-header bio bio<?php the_ID(); ?>">

            <a href="#post-<?php the_ID(); ?>"><h3 class="entry-title"><?php the_title(); ?></h3></a>
            </header>




            </div><!-- text holder -->
            </div><!-- flex item -->





<div class="modal" id="post-<?php the_ID(); ?>">
    <div class="entry-content bio-text">



        <div class="modal-image is-style-circle-mask">
            <a href="#img<?php the_ID(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"></a>
        </div>

        
        <?php
            if( has_excerpt() ){
                the_excerpt();
            }else{ 
                echo get_the_content();
            }         
        ?>  <a href="#img<?php the_ID(); ?>">close</a>

        <a href="#img<?php the_ID(); ?>" class="modal__close"> &times;</a>                              
    </div> <!-- entry-content -->
</div><!-- .modal -->




    			<?php
                }
                wp_reset_postdata();
                ?>    			
    	</div> <!-- END .container .flex-container .wrap -->
    </div> <!-- END .blog-holder -->
    <?php }