<?php
/**
 * The template for displaying Bio Category.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Benevolent
 */

get_header(); ?>



	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<header class="page-header">
				<h1 class="page-title">Staff & Faculty Bios</h1>
			</header><!-- .page-header -->
			

		<?php
		if ( have_posts() ) : get_template_part( 'sections/section-blog', get_post_format() ); ?>

			

			<?php
			

		

		

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
