<?php
/**
 * Template part for displaying single posts.
 *
 * @package Total Plus
 */
$total_plus_single_post_posted_date = get_theme_mod('total_plus_single_post_posted_date', true);
$total_plus_single_post_social_share = get_theme_mod('total_plus_single_post_social_share', true);
$total_plus_single_post_author_box = get_theme_mod('total_plus_single_post_author_box', true);
$content_display_featured_image = rwmb_meta('content_display_featured_image');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    $total_plus_show_title = get_theme_mod('total_plus_show_title', true);
    $content_display_title = rwmb_meta('content_display_title');

    if (!$total_plus_show_title && $content_display_title) {
        ?>
        <header class="entry-header">
            <?php the_title(sprintf('<h1 class="entry-title">', esc_url(get_permalink())), '</h1>'); ?>
        </header><!-- .entry-header -->
    <?php } elseif ($content_display_title) {
        ?>
        <header class="entry-header">
            <?php the_title(sprintf('<div class="entry-title">', esc_url(get_permalink())), '</div>'); ?>
        </header>
    <?php }
    ?>

    <?php if (has_post_thumbnail() && $content_display_featured_image) {
        ?>
        <figure class="entry-figure">
            <?php
            $total_plus_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'total-840x420');
            ?>
            <img src="<?php echo esc_url($total_plus_image[0]); ?>" alt="<?php echo esc_attr(get_the_title()) ?>">
        </figure>
    <?php }
    ?>

    <div class="entry-content">
        <?php if ($total_plus_single_post_posted_date) { ?>
            <div class="single-entry-meta">
                <?php total_plus_posted_on(); ?>
            </div><!-- .entry-meta -->
        <?php } ?>

        <?php
        the_content();

        wp_link_pages(array(
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'total-plus'),
            'after' => '</div>',
        ));
        ?>
    </div><!-- .entry-content -->

    <div class="entry-footer">
        <?php total_plus_entry_footer(); ?>
    </div>

    <?php if ($total_plus_single_post_social_share) { ?>
        <div class="ht-social-share">
            <span><i class="icofont-share"></i><?php esc_html_e('Share', 'total-plus'); ?></span>
                <?php
                total_plus_entry_social_share();
                ?>
        </div>
    <?php } ?>

</article><!-- #post-## -->

<?php
if ($total_plus_single_post_author_box) {
    total_plus_author_info_box();
}