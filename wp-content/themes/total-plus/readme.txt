=== Total Plus===

Contributors: HashThemes
Tags: two-columns, right-sidebar, custom-background, custom-menu, editor-style, featured-images, footer-widgets, theme-options, threaded-comments, translation-ready, portfolio, photography, holiday, custom-logo

Requires at least: 4.0
Tested up to: 4.6.0
Stable tag: 1.0.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Total WordPress Theme copyright HashThemes 2016
Total WordPress Theme is based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc.
Total is distributed under the terms of the GNU GPL v2 or later.

== Description ==

TotalPlus is a clean and beautiful WordPress theme with lots of features to make a complete website. The theme is perfect for business, photography, education, creative websites. The powerful customizer panel allows to configure the theme with live preview. The theme is SEO friendly, browser compatible, fully translation ready.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Changelog ==
= 1.2.9 - Mar 18 2019 =
* Multiple Home Page Template declaration removed
* Gutenberg support for Portfolio custom post type added

= 1.2.8 - Feb 18 2019 =
* Portfolio Type not showing in Gutenberg editor fixed.

= 1.2.7 - Jan 22 2019 =
* Google Map API not working issue fixed
* Missing Widget issue with Site Origin Pagebuilder v2.10.0 fixed

= 1.2.6 - Jan 05 2019 =
* Elementor editor not displaying content when preloader is on - fixed
* Error on Database Reset module fixed

= 1.2.5 - Dec 23 2018 =
* SVG CSS issue in admin fixed
* Customizer Home Page Sections eye icon show/hide not properly responding issue fixed
* Accordion Widget - Editor not appearing Gutenberg Editor fixed
* Gutenberg Editor added for Portfolio and MegaMenu post types
* Megamenu upgrade
* Testing with Gutenberg modules

= 1.2.4 - Nov 23 2018 =
* Added 30+ elementor Widgets
* Added Theme Option panel for more customizability
* Improvement in Demo Import
* Welcome Page added for easy navigation 

= 1.2.3 - Sep 19 2018 =
* Added HTML support for the textarea fields in widgets 

= 1.2.2 - Sep 13 2018 =
* Gradient background not working - Fixed

= 1.2.1 - Sep 10 2018 =
* New Demo Added (Construction)
* Minor stying fixes
* Customizer home section reorder glitch fixed

= 1.2.0 - Aug 29 2018 =
* Custom Post type and Page Builder conflict fixed. 

= 1.1.9 - Aug 27 2018 =
* New Demo Added (Total)
* Submenu position on hover shows in the available space with being cut off
* Option to add Google Map via Iframe added in the contact section
* More Options added for Widgets

= 1.1.8 - Aug 3 2018 =
* More robust Customizer with live preview without refresh
* Minor Design Fixes

= 1.1.7 - Jul 11 2018 =
* WooCommerce Sidebar Issue Fixed
* WooCommerce Enhancements
* Compatibility with YITH Wishlist, YITH Compare, YITH Quick View Plugins

= 1.1.6 - Jul 07 2018 =
* Archive page taking the setting of Single Post meta box setting fixed
* WooCommerce Enhancements
* Added option to filter the customizer Icons

= 1.1.5 - Jun 30 2018 =
* Social Icon Widget - output only showing url fixed
* Remove word Category, Tag, Archives from the respective archive pages
* Google Map Issue fixed (Latitude was taking Longitude value and vice-versa)
* Added Color option for title, subtitle, link color and button in the homepage sections

= 1.1.4 - Jun 23 2018 =
* Breadcumb not hiding for the custmizer setting issue fixed
* Added option for video in Call To Action Section
* Shortcodes appearing on blog and archive page fixed

= 1.1.3 - Jun 16 2018 =
* Added option in the widget page to create new widget areas
* Fixed minor typo errors
* Added option for replacing admin Logo or WordPress
* Fixed Section Seperator changes not showing bug
* Added 2 new custom section in home page
* Linkedin and Stumbled share link for single post fixed

= 1.1.2 - May 27 2018 =
* Multilanguage Translation issue fixed for the Testimonial Section and More buttons of each section.

= 1.1.1 - May 23 2018 =
* Customizer Repeater Field hanging on typing fixed
* Minor Styling Issue Fixed
* New Demo Import Added - One Page

= 1.1.0 - May 10 2018 =
* Header spacing problem when titlebar is disabled - fixed
* Auto Theme Update Bug fixed
* Google Map API field added in Customizer
* Option to align slider and banner text(left,right,center) added

= 1.0.9 - May 10 2018 =
* Auto Theme Update added
* Maintenance mode option added

= 1.0.8 - May 5 2018 =
* 6 more section title style added
* Option for a button in all section

= 1.0.7 - Apr 28 2018 =
* RTL support added

= 1.0.6 - Apr 28 2018 =
* Section Separator Added

= 1.0.5 - Apr 25 2018 =
* Sticky Header Option Added

= 1.0.4 - Apr 21 2018 =
* 3 new Blog Layout Added
* Narrow Sidebar not working Issue fixed in customizer Sidebar Layout Option

= 1.0.3 - Apr 17 2018 =
* Mega Menu Added
* Added CTA button option in Menu
* Improvement in Alpha color picker
* System Status Page added
* Improvement in Section Reorder option in Home Section

= 1.0.2 - Apr 05 2018 =
* New Demo added - Creative Agency
