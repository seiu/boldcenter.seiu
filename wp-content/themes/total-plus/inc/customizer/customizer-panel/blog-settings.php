<?php

/**
 * Total Plus Theme Customizer
 *
 * @package Total Plus
 */
$wp_customize->add_section('total_plus_blog_options_section', array(
    'title' => __('Blog/Single Post Settings', 'total-plus'),
    'priority' => 30
));

$wp_customize->add_setting('total_plus_blog_sec_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));

$wp_customize->add_control(new Total_Plus_Control_Tab($wp_customize, 'total_plus_blog_sec_nav', array(
    'type' => 'tab',
    'section' => 'total_plus_blog_options_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('BLog Page', 'total-plus'),
            'fields' => array(
                'total_plus_blog_heading',
                'total_plus_blog_layout',
                'total_plus_blog_cat',
                'total_plus_archive_content',
                'total_plus_archive_excerpt_length',
                'total_plus_archive_readmore',
                'total_plus_blog_date',
                'total_plus_blog_category',
                'total_plus_blog_tag',
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Single Post', 'total-plus'),
            'fields' => array(
                'total_plus_single_post_heading',
                'total_plus_single_post_posted_date',
                'total_plus_single_post_category',
                'total_plus_single_post_tags',
                'total_plus_single_post_social_share',
                'total_plus_single_post_pagination',
                'total_plus_single_post_author_box',
                'total_plus_single_post_related_post'
            ),
        ),
    ),
)));

$wp_customize->add_setting('total_plus_blog_layout', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => 'blog-layout1'
));

$wp_customize->add_control(new Total_Plus_Image_Select($wp_customize, 'total_plus_blog_layout', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Blog Layout', 'total-plus'),
    'image_path' => $imagepath . '/inc/customizer/images/',
    'choices' => array(
        'blog-layout1' => esc_html__('Layout 1', 'total-plus'),
        'blog-layout2' => esc_html__('Layout 2', 'total-plus'),
        'blog-layout3' => esc_html__('Layout 3', 'total-plus'),
        'blog-layout4' => esc_html__('Layout 4', 'total-plus')
    )
)));

$wp_customize->add_setting('total_plus_blog_cat', array(
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control(new Total_Plus_Customize_Checkbox_Multiple($wp_customize, 'total_plus_blog_cat', array(
    'label' => esc_html__('Exclude Category', 'total-plus'),
    'section' => 'total_plus_blog_options_section',
    'choices' => $total_plus_cat,
    'description' => esc_html__('Post with selected category will not display in the blog page', 'total-plus')
)));

$wp_customize->add_setting('total_plus_archive_content', array(
    'default' => 'excerpt',
    'sanitize_callback' => 'total_plus_sanitize_choices'
));

$wp_customize->add_control('total_plus_archive_content', array(
    'section' => 'total_plus_blog_options_section',
    'type' => 'radio',
    'label' => esc_html__('Archive Content', 'total-plus'),
    'choices' => array(
        'full-content' => esc_html__('Full Content', 'total-plus'),
        'excerpt' => esc_html__('Excerpt', 'total-plus')
    )
));

$wp_customize->add_setting('total_plus_archive_excerpt_length', array(
    'sanitize_callback' => 'absint',
    'default' => 100
));

$wp_customize->add_control(new Total_Plus_Range_Control($wp_customize, 'total_plus_archive_excerpt_length', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Excerpt Length (words)', 'total-plus'),
    'options' => array(
        'min' => 50,
        'max' => 200,
        'step' => 1
    )
)));

$wp_customize->add_setting('total_plus_archive_readmore', array(
    'default' => esc_html__('Read More', 'total-plus'),
    'sanitize_callback' => 'total_plus_sanitize_text'
));

$wp_customize->add_control('total_plus_archive_readmore', array(
    'section' => 'total_plus_blog_options_section',
    'type' => 'text',
    'label' => esc_html__('Read More Text', 'total-plus')
));

$wp_customize->add_setting('total_plus_blog_date', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_blog_date', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Posted Date', 'total-plus')
)));

$wp_customize->add_setting('total_plus_blog_category', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_blog_category', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Category', 'total-plus')
)));

$wp_customize->add_setting('total_plus_blog_tag', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_blog_tag', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Tag', 'total-plus')
)));

$wp_customize->add_setting('total_plus_single_post_posted_date', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_single_post_posted_date', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Author/Date/Comment', 'total-plus')
)));

$wp_customize->add_setting('total_plus_single_post_category', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_single_post_category', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Category', 'total-plus')
)));

$wp_customize->add_setting('total_plus_single_post_tags', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_single_post_tags', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Tags', 'total-plus')
)));

$wp_customize->add_setting('total_plus_single_post_social_share', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_single_post_social_share', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Social Share', 'total-plus')
)));

$wp_customize->add_setting('total_plus_single_post_pagination', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_single_post_pagination', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Prev Next Navigation', 'total-plus')
)));

$wp_customize->add_setting('total_plus_single_post_author_box', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_single_post_author_box', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Author Box', 'total-plus')
)));

$wp_customize->add_setting('total_plus_single_post_related_post', array(
    'sanitize_callback' => 'total_plus_sanitize_text',
    'default' => true
));

$wp_customize->add_control(new Total_Plus_Checkbox_Control($wp_customize, 'total_plus_single_post_related_post', array(
    'section' => 'total_plus_blog_options_section',
    'label' => esc_html__('Display Related Posts', 'total-plus')
)));
