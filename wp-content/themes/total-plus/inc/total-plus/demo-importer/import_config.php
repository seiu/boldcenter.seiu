<?php

/*
 * Config file with each demo data
 */

$demos = array(
    'total' => array(
        'slug' => 'total',
        'name' => 'Total',
        'external_url' => 'https://hashthemes.com/import-files/total-plus/total.zip',
        'image' => 'https://hashthemes.com/import-files/total-plus/screen/total.jpg',
        'preview_url' => 'https://demo.hashthemes.com/total-plus/total/',
        'tags' => array(
            'creative' => 'Creative',
            'portfolio' => 'Portfolio',
        ),
        'plugins' => array(
            'contact-form-7' => array(
                'name' => 'Contact Form 7',
                'source' => 'wordpress',
                'file_path' => 'contact-form-7/wp-contact-form-7.php'
            ),
            'siteorigin-panels' => array(
                'name' => 'Page Builder by SiteOrigin',
                'source' => 'wordpress',
                'file_path' => 'siteorigin-panels/siteorigin-panels.php'
            ),
            'so-widgets-bundle' => array(
                'name' => 'SiteOrigin Widgets Bundle',
                'source' => 'wordpress',
                'file_path' => 'so-widgets-bundle/so-widgets-bundle.php'
            ) 
        ),
        'menuArray' => array(
            'primary' => 'Main Menu'
        )
    ),
    'main-demo' => array(
        'slug' => 'main-demo',
        'name' => 'Main Demo',
        'external_url' => 'https://hashthemes.com/import-files/total-plus/main-demo.zip',
        'image' => 'https://hashthemes.com/import-files/total-plus/screen/main-demo.jpg',
        'preview_url' => 'https://demo.hashthemes.com/total-plus/main/',
        'tags' => array(
            'creative' => 'Creative',
            'portfolio' => 'Portfolio',
        ),
        'plugins' => array(
            'contact-form-7' => array(
                'name' => 'Contact Form 7',
                'source' => 'wordpress',
                'file_path' => 'contact-form-7/wp-contact-form-7.php'
            ),
            'siteorigin-panels' => array(
                'name' => 'Page Builder by SiteOrigin',
                'source' => 'wordpress',
                'file_path' => 'siteorigin-panels/siteorigin-panels.php'
            ),
            'so-widgets-bundle' => array(
                'name' => 'SiteOrigin Widgets Bundle',
                'source' => 'wordpress',
                'file_path' => 'so-widgets-bundle/so-widgets-bundle.php'
            )
        ),
        'menuArray' => array(
            'primary' => 'Main Menu'
        )
    ),
    'creative-agency' => array(
        'slug' => 'creative-agency',
        'name' => 'Creative Agency',
        'external_url' => 'https://hashthemes.com/import-files/total-plus/creative-agency.zip',
        'image' => 'https://hashthemes.com/import-files/total-plus/screen/creative-agency.jpg',
        'preview_url' => 'https://demo.hashthemes.com/total-plus/creative-agency',
        'tags' => array(
            'creative' => 'Creative',
            'portfolio' => 'Portfolio',
        ),
        'plugins' => array(
            'contact-form-7' => array(
                'name' => 'Contact Form 7',
                'source' => 'wordpress',
                'file_path' => 'contact-form-7/wp-contact-form-7.php'
            ),
            'siteorigin-panels' => array(
                'name' => 'Page Builder by SiteOrigin',
                'source' => 'wordpress',
                'file_path' => 'siteorigin-panels/siteorigin-panels.php'
            ),
            'so-widgets-bundle' => array(
                'name' => 'SiteOrigin Widgets Bundle',
                'source' => 'wordpress',
                'file_path' => 'so-widgets-bundle/so-widgets-bundle.php'
            ),
            'revslider' => array(
                'name' => 'Slider Revolution',
                'source' => 'remote',
                'file_path' => 'revslider/revslider.php',
                'location' => 'https://hashthemes.com/import-files/plugins/revslider.zip'
            ),
        ),
        'menuArray' => array(
            'primary' => 'Main Menu'
        )
    ),
    'one-page' => array(
        'slug' => 'one-page',
        'name' => 'One Page',
        'external_url' => 'https://hashthemes.com/import-files/total-plus/one-page.zip',
        'image' => 'https://hashthemes.com/import-files/total-plus/screen/one-page.jpg',
        'preview_url' => 'https://demo.hashthemes.com/total-plus/one-page',
        'tags' => array(
            'creative' => 'Creative',
            'portfolio' => 'Portfolio',
        ),
        'plugins' => array(
            'siteorigin-panels' => array(
                'name' => 'Page Builder by SiteOrigin',
                'source' => 'wordpress',
                'file_path' => 'siteorigin-panels/siteorigin-panels.php'
            ),
            'so-widgets-bundle' => array(
                'name' => 'SiteOrigin Widgets Bundle',
                'source' => 'wordpress',
                'file_path' => 'so-widgets-bundle/so-widgets-bundle.php'
            )
        ),
        'menuArray' => array(
            'primary' => 'Main Menu'
        )
    ),
    'construction' => array(
        'slug' => 'construction',
        'name' => 'Construction',
        'external_url' => 'https://hashthemes.com/import-files/total-plus/construction.zip',
        'image' => 'https://hashthemes.com/import-files/total-plus/screen/construction.jpg',
        'preview_url' => 'https://demo.hashthemes.com/total-plus/construction',
        'tags' => array(
            'creative' => 'Creative',
            'portfolio' => 'Portfolio',
        ),
        'plugins' => array(
            'contact-form-7' => array(
                'name' => 'Contact Form 7',
                'source' => 'wordpress',
                'file_path' => 'contact-form-7/wp-contact-form-7.php'
            ),
            'siteorigin-panels' => array(
                'name' => 'Page Builder by SiteOrigin',
                'source' => 'wordpress',
                'file_path' => 'siteorigin-panels/siteorigin-panels.php'
            ),
            'so-widgets-bundle' => array(
                'name' => 'SiteOrigin Widgets Bundle',
                'source' => 'wordpress',
                'file_path' => 'so-widgets-bundle/so-widgets-bundle.php'
            ),
            'revslider' => array(
                'name' => 'Slider Revolution',
                'source' => 'remote',
                'file_path' => 'revslider/revslider.php',
                'location' => 'https://hashthemes.com/import-files/plugins/revslider.zip'
            ),
            'newsletter' => array(
                'name' => 'Newsletter',
                'source' => 'wordpress',
                'file_path' => 'newsletter/plugin.php'
            ),
        ),
        'menuArray' => array(
            'primary' => 'Main Menu'
        )
    )
);

return $demos;
