<?php
/**
 * @package Total Plus
 */
add_action('widgets_init', 'total_plus_register_instagram');

function total_plus_register_instagram() {
    register_widget('total_plus_instagram');
}

class total_plus_instagram extends WP_Widget {

    public function __construct() {
        parent::__construct(
                'total_plus_instagram', '&bull; Total : Instagram', array(
            'description' => __('A widget to display Instagram Images', 'total-plus')
                )
        );
    }

    /**
     * Helper function that holds widget fields
     * Array is used in update and form functions
     */
    private function widget_fields() {
        $fields = array(
            'title' => array(
                'total_plus_widgets_name' => 'title',
                'total_plus_widgets_title' => __('Title', 'total-plus'),
                'total_plus_widgets_field_type' => 'text'
            ),
            'accesstoken' => array(
                'total_plus_widgets_name' => 'accesstoken',
                'total_plus_widgets_title' => __('Access Token', 'total-plus'),
                'total_plus_widgets_field_type' => 'text',
                'total_plus_widgets_description' => 'More Detail: <a href="https://rudrastyh.com/tools/access-token" target="_blank">Click Here</a>'
            ),
            'user_id' => array(
                'total_plus_widgets_name' => 'user_id',
                'total_plus_widgets_title' => __('User Id', 'total-plus'),
                'total_plus_widgets_field_type' => 'text',
                'total_plus_widgets_description' => 'More Detail: <a href="https://codeofaninja.com/tools/find-instagram-user-id" target="_blank">Click Here</a>'
            ),
            'sort_by' => array(
                'total_plus_widgets_name' => 'sort_by',
                'total_plus_widgets_title' => __('Sort By', 'total-plus'),
                'total_plus_widgets_field_type' => 'select',
                'total_plus_widgets_field_options' => array(
                    'none' => esc_html__('None', 'total-plus'),
                    'most-recent' => esc_html__('Newest to oldest', 'total-plus'),
                    'least-recent' => esc_html__('Oldest to newest', 'total-plus'),
                    'most-liked' => esc_html__('Highest # of likes to lowest', 'total-plus'),
                    'least-liked' => esc_html__('Lowest # likes to highest', 'total-plus'),
                    'most-commented' => esc_html__('Highest # of comments to lowest', 'total-plus'),
                    'least-commented' => esc_html__('Lowest # of comments to highest', 'total-plus'),
                    'random' => esc_html__('Random', 'total-plus')
                )
            ),
            'limit' => array(
                'total_plus_widgets_name' => 'limit',
                'total_plus_widgets_title' => __('No of Image to Display', 'total-plus'),
                'total_plus_widgets_field_type' => 'number',
                'total_plus_widgets_default' => '10'
            ),
            'resolution' => array(
                'total_plus_widgets_name' => 'resolution',
                'total_plus_widgets_title' => __('Image Size', 'total-plus'),
                'total_plus_widgets_field_type' => 'select',
                'total_plus_widgets_field_options' => array(
                    'thumbnail' => esc_html__('Small', 'total-plus'),
                    'low_resolution' => esc_html__('Medium', 'total-plus'),
                    'standard_resolution' => esc_html__('Large', 'total-plus')
                ),
                'total_plus_widgets_default' => 'thumbnail'
            ),
            'row_height' => array(
                'total_plus_widgets_name' => 'row_height',
                'total_plus_widgets_title' => esc_html__('Row Height', 'total-plus'),
                'total_plus_widgets_field_type' => 'number',
                'total_plus_widgets_default' => 120,
                'total_plus_widgets_description' => esc_html__('The height determines the no of image in row', 'total-plus')
            ),
            'enable_space' => array(
                'total_plus_widgets_name' => 'enable_space',
                'total_plus_widgets_title' => esc_html__('Enable Space Between Images', 'total-plus'),
                'total_plus_widgets_field_type' => 'checkbox'
            ),
            'likes_count' => array(
                'total_plus_widgets_name' => 'likes_count',
                'total_plus_widgets_title' => __('Show Like Count', 'total-plus'),
                'total_plus_widgets_field_type' => 'checkbox'
            )
        );

        return $fields;
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance) {
        extract($args);

        $title = isset($instance['title']) ? $instance['title'] : '';
        $user_id = isset($instance['user_id']) ? $instance['user_id'] : '';
        $accesstoken = isset($instance['accesstoken']) ? $instance['accesstoken'] : '';
        $sort_by = isset($instance['sort_by']) ? $instance['sort_by'] : 'none';
        $limit = isset($instance['limit']) ? $instance['limit'] : '10';
        $resolution = isset($instance['resolution']) ? $instance['resolution'] : 'thumbnail';
        $row_height = isset($instance['row_height']) ? $instance['row_height'] : 120;
        $enable_space = (isset($instance['enable_space']) && $instance['enable_space']) ? 'enable-space' : '';
        $likes_count = isset($instance['likes_count']) ? $instance['likes_count'] : '';
        $margin = (isset($instance['enable_space']) && $instance['enable_space']) ? 10 : 0;
        $likes = '';

        echo $before_widget;
        ?>
        <div class="ht-instagram-widget">
            <?php
            if (!empty($title)):
                echo $before_title . apply_filters('widget_title', $title) . $after_title;
            endif;
            ?>
            <div id="<?php echo esc_attr($widget_id); ?>wrapper" class="ht-instagram-widget-wrap"></div>

        </div>
        <?php
        if ($likes_count) {
            $likes = '<div class="ht-iw-likes"><i class="icofont-heart"></i> {{likes}}</div>';
        }

        if (!empty($user_id) && !empty($accesstoken)) {
            ?>
            <script type="text/javascript">
                jQuery(function ($) {
                    var feed = new Instafeed({
                        get: 'user',
                        target: '<?php echo $widget_id; ?>wrapper',
                        userId: '<?php echo absint($user_id); ?>',
                        sortBy: '<?php echo $sort_by; ?>',
                        limit: '<?php echo $limit; ?>',
                        resolution: '<?php echo $resolution; ?>',
                        accessToken: '<?php echo $accesstoken; ?>',
                        template: '<a href="{{link}}" target="_blank"><img src="{{image}}" /><?php echo $likes; ?></a>',
                        after: function () {
                            $('#<?php echo esc_attr($widget_id); ?>wrapper').justifiedGallery({
                                rowHeight: <?php echo absint($row_height); ?>,
                                lastRow: 'nojustify',
                                margins: <?php echo $margin; ?>
                            });
                        }
                    });
                    feed.run();
                });
            </script>
            <?php
        }
        echo $after_widget;
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param   array   $new_instance   Values just sent to be saved.
     * @param   array   $old_instance   Previously saved values from database.
     *
     * @uses    total_plus_widgets_updated_field_value()        defined in widget-fields.php
     *
     * @return  array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;

        $widget_fields = $this->widget_fields();

        // Loop through fields
        foreach ($widget_fields as $widget_field) {
            extract($widget_field);
            if (!total_plus_exclude_widget_update($total_plus_widgets_field_type)) {
                $new = isset($new_instance[$total_plus_widgets_name]) ? $new_instance[$total_plus_widgets_name] : '';
                // Use helper function to get updated field values
                $instance[$total_plus_widgets_name] = total_plus_widgets_updated_field_value($widget_field, $new);
            }
        }

        return $instance;
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param   array $instance Previously saved values from database.
     *
     * @uses    total_plus_widgets_show_widget_field()      defined in widget-fields.php
     */
    public function form($instance) {
        $widget_fields = $this->widget_fields();
        // Loop through fields
        foreach ($widget_fields as $widget_field) {
            // Make array elements available as variables
            extract($widget_field);

            if (!total_plus_exclude_widget_update($total_plus_widgets_field_type)) {
                $total_plus_widgets_field_value = !empty($instance[$total_plus_widgets_name]) ? $instance[$total_plus_widgets_name] : '';
            }else{
                $total_plus_widgets_field_value = '';
            }

            total_plus_widgets_show_widget_field($this, $widget_field, $total_plus_widgets_field_value);
        }
    }

}
