<?php

/**
 * Total Plus Theme Customizer
 *
 * @package Total Plus
 */
$wp_customize->get_section('colors')->title = esc_html__('Color Settings', 'total-plus');
$wp_customize->get_section('colors')->priority = 10;
